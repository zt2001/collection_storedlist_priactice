﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList list = new SortedList();

            list.Add(2, "猴子点灯");

            list.Add(1, "猩猩点灯");

            list.Add(3, "建龙点灯");

            Console.WriteLine("点灯动物如下");
            foreach (DictionaryEntry item in list)
            {
                Console.WriteLine("{0}  {1}", item.Key, item.Value);
            }

            Console.WriteLine("请选择那个动物点灯");

            int id = int.Parse(Console.ReadLine());

            bool flag = list.ContainsKey(id);

            if (flag)
            {
                string name = list[id].ToString();
                Console.WriteLine("你选择点灯的动物是{0}", name);
            }
            else
            {
                Console.WriteLine("你选择点灯的动物不存在");
            }

            
        }
    }


}
