﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedLists
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(2, "超级爱吃");
            sortedList.Add(1, "小胖");
            sortedList.Add(3, "大橙子");
            //根据编号查询名称
            Console.WriteLine("请输入编号:");
            int Id = int.Parse(Console.ReadLine());
            bool Tobl = sortedList.ContainsKey(Id);
            if (Tobl)
            {
                string name =sortedList[Id].ToString();
                Console.WriteLine("名称为：{0}",name);
            }
            else
            {
                Console.WriteLine("不存在该名称或编号！");
            }

            Console.WriteLine();
            //输出所有编号与名称
            foreach(DictionaryEntry ai in sortedList)
            {
                int key = (int)ai.Key;
                string value = ai.Value.ToString();
                Console.WriteLine("编号为：{0}  名称为：{1}",key,value);
            }











        }
    }
}
