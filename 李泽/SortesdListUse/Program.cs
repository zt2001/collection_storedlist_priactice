﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortesdListUse
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList list = new SortedList();
            list.Add(1, "西红市首富");//添加数据
            list.Add(2, "唐人街探案");
            list.Add(3, "唐人街探案2");
            list.Add(4, "战狼");
            list.Add(5, "战狼2");
            list.Add(6, "警察故事");
            Console.WriteLine("请输入需要购买的电影序号");
            int sa = int.Parse(Console.ReadLine());
            bool flag = list.ContainsKey(sa);//判断是否存在键名
            if (flag)
            {
                string name = list[sa].ToString();
                Console.WriteLine("您要购买的电影是：{0}",name);
            }
            else
            {
                Console.WriteLine("您购买的电影不存在");
            }
            Console.WriteLine("在播电影如下");
            foreach (DictionaryEntry a in list)
            {
                int key = (int)a.Key;//此处key为键名
                string value = a.Value.ToString();//value为值
                Console.WriteLine("电影编号{0}电影名{1}",key,value);

            }
        }
    }
}
