﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedListLx
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sor = new SortedList();
            sor.Add(1,"小张");
            sor.Add(2,"小李");
            sor.Add(3, "小刘");
            Console.WriteLine("请输入挂科编号：");
            int id = int.Parse(Console.ReadLine());
            bool flag = sor.ContainsKey(id);
            Console.WriteLine(sor.ContainsKey(id));
            if (flag)
            {
                string name = sor[id].ToString();
                Console.WriteLine("您查找的患者姓名为：{0}",name);
            }
            else
            {
                Console.WriteLine("您查找的挂号编号不存在！");
            }
            foreach (DictionaryEntry i in sor)
            {
                Console.WriteLine("挂号编号:{0},姓名:{1}",i.Key,i.Value);
            }
        }
    }
}
